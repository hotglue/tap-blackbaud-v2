"""REST client handling, including BlackbaudV2Stream base class."""

from __future__ import annotations

from singer_sdk.pagination import BaseHATEOASPaginator



class BlackbaudV2Paginator(BaseHATEOASPaginator):
    def get_next_url(self, response):
        return response.json().get("next_link")


class BlackbaudV2PaginatorForChildStreams(BaseHATEOASPaginator):
    """
    Paginator instance that is used for child streams. This is needed because
    we don't want to iterate through the pages from the originally requested URL.
    """

    def get_next_url(self, response):
        return None