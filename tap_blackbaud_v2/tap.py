"""BlackbaudV2 tap class."""

from __future__ import annotations

import os

from singer_sdk import Tap
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_blackbaud_v2 import streams
from tap_blackbaud_v2.auth import BlackbaudV2Authenticator

import requests
import backoff

CHILD_STREAMS = [
    streams.ConstituentsByListStream
]
SPECIFIC_STREAMS = [
    streams.EducationsStream,
    streams.AllConstituentsStream,
    streams.ConstituentListsStream,
    streams.ConstituentsByListStream,
    # streams.ConstituentLifetimeGivingStream
    # streams.ConstituentFundraiserAssignmentStream
]

class TapBlackbaudV2(Tap):
    """BlackbaudV2 tap class."""

    name = "tap-blackbaud-v2"
    tap_name = name
    config_file = os.getcwd() + "/config.json"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._tap = self

    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("refresh_token", th.StringType, required=True),
        th.Property("redirect_uri", th.StringType, required=True),
        th.Property("subscription_key", th.StringType, required=True),
        th.Property("start_date", th.DateTimeType)
    ).to_dict()

    @property
    def authenticator(self) -> BlackbaudV2Authenticator:
        """Return a new authenticator object."""
        return BlackbaudV2Authenticator(self)

    def validate_response(self, response):
        if response.status_code == 200:
            return response

        try:
            content = response.json()
        except:
            content = {}

        if response.status_code == 400 and content.get("error") == "invalid_grant":
            self.authenticator.update_access_token()
        else:
            response.raise_for_status()

        return response

    def gen_headers(self):
        headers = {}
        if self.config.get("subscription_key"):
            headers['Bb-Api-Subscription-Key'] = self.config["subscription_key"]

        return headers

    @backoff.on_exception(backoff.expo, requests.exceptions.RequestException, max_time=60)
    def _get_blackbaud_lists(self):
        """Get all constituent lists from Blackbaud."""

        headers = self.gen_headers()
        headers.update(self.authenticator.auth_headers)
        response = requests.get(
            "https://api.sky.blackbaud.com/list/v1/lists?list_type=Constituent",
            headers=headers,
        )
        response = self.validate_response(response)
        return response.json()

    def discover_streams(self) -> list[streams.BlackbaudV2Stream]:
        lists = self._get_blackbaud_lists()

        return [stream_class(tap=self) for stream_class in SPECIFIC_STREAMS]


if __name__ == "__main__":
    TapBlackbaudV2.cli()
