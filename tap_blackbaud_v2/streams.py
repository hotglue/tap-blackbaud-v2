"""Stream type classes for tap-blackbaud-v2."""

from __future__ import annotations

import typing as t
from pathlib import Path
from typing import Optional
from urllib.parse import parse_qsl

import singer_sdk._singerlib.messages as singer

from singer_sdk import typing as th
from singer_sdk.mapper import CustomStreamMap
from singer_sdk.helpers.jsonpath import extract_jsonpath
from tap_blackbaud_v2.client import BlackbaudV2Stream
from tap_blackbaud_v2.paginators import BlackbaudV2PaginatorForChildStreams
class ConstituentListsStream(BlackbaudV2Stream):
    """
    https://developer.sky.blackbaud.com/api#api=list&operation=GetLists
    """
    name = "constituent_lists"

    path = "/list/v1/lists?list_type=Constituent"

    primary_keys = ["id"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("description", th.StringType),
        th.Property("record_count", th.IntegerType),
        th.Property("date_modified", th.DateTimeType),
        th.Property("last_modified_by_user_name", th.StringType),
        th.Property("last_modified_by_user_id", th.StringType),
        th.Property("is_public", th.BooleanType)
    ).to_dict()


class ConstituentsByListStream(BlackbaudV2Stream):
    """
    Docs: https://developer.sky.blackbaud.com/api#api=56b76470069a0509c8f1c5b3&operation=ListConstituents
    """
    name = "constituents_by_selected_list"
    path = "/constituent/v1/constituents/?limit=1"

    # This is the real schema that the stream will use to parse the responses
    json_schema = th.PropertiesList(
            th.Property("id", th.StringType),
            th.Property("list_id", th.StringType),
            th.Property("address", th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("address_lines", th.StringType),
                th.Property("city", th.StringType),
                th.Property("constituent_id", th.StringType),
                th.Property("country", th.StringType),
                th.Property("county", th.StringType),
                th.Property("formatted_address", th.StringType),
                th.Property("inactive", th.BooleanType),
                th.Property("postal_code", th.StringType),
                th.Property("preferred", th.BooleanType),
                th.Property("state", th.StringType)
            )),
            th.Property("age", th.IntegerType),
            th.Property("birthdate", th.ObjectType(
                th.Property("d", th.IntegerType),
                th.Property("m", th.IntegerType),
                th.Property("y", th.IntegerType)
            )),
            th.Property("date_added", th.DateTimeType),
            th.Property("date_modified", th.DateTimeType),
            th.Property("deceased", th.BooleanType),
            th.Property("email", th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("address", th.StringType),
                th.Property("constituent_id", th.StringType),
                th.Property("do_not_email", th.BooleanType),
                th.Property("inactive", th.BooleanType),
                th.Property("primary", th.BooleanType),
                th.Property("type", th.StringType)
            )),
            th.Property("first", th.StringType),
            th.Property("fundraiser_status", th.StringType),
            th.Property("gender", th.StringType),
            th.Property("gives_anonymously", th.BooleanType),
            th.Property("inactive", th.BooleanType),
            th.Property("last", th.StringType),
            th.Property("lookup_id", th.StringType),
            th.Property("middle", th.StringType),
            th.Property("name", th.StringType),
            th.Property("phone", th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("constituent_id", th.StringType),
                th.Property("do_not_call", th.BooleanType),
                th.Property("inactive", th.BooleanType),
                th.Property("number", th.StringType),
                th.Property("primary", th.BooleanType),
                th.Property("type", th.StringType)
            )),
            th.Property("suffix", th.StringType),
            th.Property("title", th.StringType),
            th.Property("type", th.StringType)
        ).to_dict()


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self._tap.catalog != {}:
            self._schema = self.json_schema

    def get_new_paginator(self):
        return BlackbaudV2PaginatorForChildStreams()

    def generate_blackbaud_lists_schema(self):
        lists = self._tap._get_blackbaud_lists()
        prop_lists = []

        for list_obj in lists.get("value", []):
            prop_lists.append(th.Property(list_obj["name"], th.StringType, description=list_obj["id"]))

        return th.PropertiesList(*prop_lists).to_dict()

    @property
    def schema(self):
        """
        Generates a catalog based on the lists from Blackbaud.
        """
        if self._tap.catalog == {}:
            return self.generate_blackbaud_lists_schema()

        return self.json_schema

    @property
    def selected_lists(self):
        """
        Custom function created with the purpose of getting which lists are selected from the
        field map and making it easier to iterate over them on parse_response.
        """
        selected_lists = []
        blackbaud_schema = self.generate_blackbaud_lists_schema()
        for list_obj in self.metadata.keys():
            if self.metadata[list_obj].selected == True and len(list_obj) != 0:
                selected_lists.append((
                    blackbaud_schema["properties"].get(list_obj[1], {}).get("description"),
                    list_obj[1]
                ))
        return selected_lists

    def _write_schema_message(self) -> None:
        singer.write_message(singer.SchemaMessage(
            stream=self.name,
            schema = self.json_schema,
            key_properties=[]
        ))

    def parse_response(self, response):
        # gets the complete URL for the request
        self.logger.info("Custom parse response on ConstituentsByListStream")
        complete_url = self.get_url({}).split("?")[0]
        for list_obj in self.selected_lists:
            # Iterates through every list in selected lists and make
            # a request to the API. For each list, it tries to get
            # if we have a next link, if we do, it will continue to
            # request until there is no next link.

            has_next = True
            list_url = complete_url + f"?list_id={list_obj[0]}"
            while has_next:
                self.logger.info(f"Requesting {list_url}")
                headers = self._tap.gen_headers()
                headers.update(self._tap.authenticator.auth_headers)
                prepared_req = self.build_prepared_request(method="GET", url=list_url, headers=headers)
                req = self._request(prepared_req, {})
                response_json = req.json()
                for record in response_json.get("value", []):
                    record["list_id"] = list_obj[0]
                    yield record

                has_next_link = response_json.get("next_link") == None
                if has_next_link:
                    has_next = False
                    break

                list_url = response_json.get("next_link")


class AllConstituentsStream(BlackbaudV2Stream):
    name = "all_constituents"
    path = "/constituent/v1/constituents"
    primary_keys = ["id"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("address", th.ObjectType(
            th.Property("id", th.StringType),
            th.Property("address_lines", th.StringType),
            th.Property("city", th.StringType),
            th.Property("country", th.StringType),
            th.Property("county", th.StringType),
            th.Property("formatted_address", th.StringType),
            th.Property("inactive", th.BooleanType),
            th.Property("postal_code", th.StringType),
            th.Property("preferred", th.BooleanType),
            th.Property("state", th.StringType)
        )),
        th.Property("age", th.IntegerType),
        th.Property("birthdate", th.ObjectType(
            th.Property("d", th.IntegerType),
            th.Property("m", th.IntegerType),
            th.Property("y", th.IntegerType)
        )),
        th.Property("date_added", th.DateTimeType),
        th.Property("date_modified", th.DateTimeType),
        th.Property("deceased", th.BooleanType),
        th.Property("email", th.ObjectType(
            th.Property("id", th.StringType),
            th.Property("address", th.StringType),
            th.Property("do_not_email", th.BooleanType),
            th.Property("inactive", th.BooleanType),
            th.Property("primary", th.BooleanType),
            th.Property("type", th.StringType)
        )),
        th.Property("first", th.StringType),
        th.Property("fundraiser_status", th.StringType),
        th.Property("gender", th.StringType),
        th.Property("gives_anonymously", th.BooleanType),
        th.Property("inactive", th.BooleanType),
        th.Property("last", th.StringType),
        th.Property("lookup_id", th.StringType),
        th.Property("middle", th.StringType),
        th.Property("name", th.StringType),
        th.Property("online_presence", th.ObjectType (
            th.Property("id", th.StringType),
            th.Property("address", th.StringType),
            th.Property("inactive", th.BooleanType),
            th.Property("primary", th.BooleanType),
            th.Property("type", th.StringType)
        )),
        th.Property("phone", th.ObjectType(
            th.Property("id", th.StringType),
            th.Property("do_not_call", th.BooleanType),
            th.Property("inactive", th.BooleanType),
            th.Property("number", th.StringType),
            th.Property("primary", th.BooleanType),
            th.Property("type", th.StringType)
        )),
        th.Property("preferred_name", th.StringType),
        th.Property("spouse", th.ObjectType (
            th.Property("id", th.StringType),
            th.Property("first", th.StringType),
            th.Property("last", th.StringType),
            th.Property("is_head_of_household", th.BooleanType)
        )),
        th.Property("title", th.StringType),
        th.Property("type", th.StringType),
        th.Property("lifetime_giving", th.ObjectType(
            th.Property("consecutive_years_given", th.IntegerType),
            th.Property("total_committed_matching_gifts", th.NumberType),
            th.Property("total_giving", th.NumberType),
            th.Property("total_pledge_balance", th.NumberType),
            th.Property("total_received_giving", th.NumberType),
            th.Property("total_received_matching_gifts", th.NumberType),
            th.Property("total_soft_credits", th.NumberType),
            th.Property("total_years_given", th.IntegerType),
        )),
        th.Property("fundraiser_assignment_list", th.ArrayType(
            th.ObjectType(
                th.Property("id", th.StringType),             # required
                th.Property("campaign_id", th.StringType),    # required
                th.Property("fundraiser_id", th.StringType),  # required
                th.Property("appeal_id", th.StringType),      # optional
                th.Property("fund_id", th.StringType),        # optional
                th.Property("amount", th.NumberType),         # required
                th.Property("start", th.DateTimeType),        # optional
                th.Property("end", th.DateTimeType),          # optional
                th.Property("type", th.StringType),           # required
            )
        ))
    ).to_dict()

    def get_child_context(self, record, context):
        return {
            "constituent_id": record["id"]
        }


class EducationsStream(BlackbaudV2Stream):
    """
    Docs: https://developer.sky.blackbaud.com/api#api=56b76470069a0509c8f1c5b3&operation=ListConstituentEducationsAllConstituents
    """

    name = "educations"
    path = "/constituent/v1/educations"
    primary_keys = ["id"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("constituent_id", th.StringType),
        th.Property("campus", th.StringType),
        th.Property("class_of", th.StringType),
        th.Property("date_added", th.StringType),
        th.Property("date_entered", th.ObjectType(
            th.Property("d", th.IntegerType),
            th.Property("m", th.IntegerType),
            th.Property("y", th.IntegerType)
        )),
        th.Property("date_graduated", th.ObjectType(
            th.Property("d", th.IntegerType),
            th.Property("m", th.IntegerType),
            th.Property("y", th.IntegerType)
        )),
        th.Property("date_modified", th.StringType),
        th.Property("degree", th.StringType),
        th.Property("majors", th.ArrayType(th.StringType)),
        th.Property("minors", th.ArrayType(th.StringType)),
        th.Property("primary", th.BooleanType),
        th.Property("school", th.StringType),
        th.Property("type", th.StringType)
    ).to_dict()


class ConstituentLifetimeGivingStream(BlackbaudV2Stream):
    def __init__(self, parent_stream_type=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.replication_key = None
        self.path = "/constituent/v1/constituents/{constituent_id}/givingsummary/lifetimegiving"
        self.records_jsonpath = "$"
        self.parent_stream_type = AllConstituentsStream
        if parent_stream_type:
            self.parent_stream_type = parent_stream_type

    schema = th.PropertiesList(
        th.Property("consecutive_years_given", th.IntegerType),
        th.Property("constituent_id", th.StringType),
        th.Property(
            "total_committed_matching_gifts", th.ObjectType(
                th.Property("value", th.IntegerType
            ))),
        th.Property(
            "total_giving", th.ObjectType(
                th.Property("value", th.IntegerType
            ))),
        th.Property(
            "total_pledge_balance", th.ObjectType(
                th.Property("value", th.IntegerType
            ))),
        th.Property(
            "total_received_giving", th.ObjectType(
                th.Property("value", th.IntegerType
            ))),
        th.Property(
            "total_received_matching_gifts", th.ObjectType(
                th.Property("value", th.IntegerType
            ))),
        th.Property(
            "total_soft_credits", th.ObjectType(
                th.Property("value", th.IntegerType
            ))),
        th.Property("total_years_given", th.IntegerType),
    ).to_dict()

    def parse_response(self, response):
        return super().parse_response(response)


class ConstituentFundraiserAssignmentStream(BlackbaudV2Stream):
    def __init__(self, parent_stream_type=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.replication_key = None
        self.path = "/constituent/v1/constituents/{constituent_id}/fundraiserassignments?include_inactive=true"
        self.records_jsonpath = "$"
        self.parent_stream_type = AllConstituentsStream
        if parent_stream_type:
            self.parent_stream_type = parent_stream_type

    schema = th.PropertiesList(
        th.Property("schema", th.StringType)
    ).to_dict()

    def parse_response(self, response):
        return super().parse_response(response)
