"""BlackbaudV2 Authentication."""

from __future__ import annotations

import os
import json
import requests

from singer_sdk.helpers._util import utc_now
from singer_sdk.authenticators import OAuthAuthenticator, SingletonMeta


class BlackbaudV2Authenticator(OAuthAuthenticator, metaclass=SingletonMeta):
    def __init__(self, stream):
        super().__init__(stream=stream)
        self._auth_endpoint = "https://oauth2.sky.blackbaud.com/token"
        self._tap = stream._tap

    @property
    def auth_endpoint(self):
        return self._auth_endpoint

    @property
    def oauth_request_body(self) -> dict:
        return {
            'grant_type': 'refresh_token',
            'client_id': self.config["client_id"],
            'client_secret': self.config["client_secret"],
            'refresh_token': self.config["refresh_token"] if not self.refresh_token else self.refresh_token,
            'redirect_uri': self.config["redirect_uri"]
        }

    def update_access_token(self):
        """Update `access_token` along with: `last_refreshed` and `expires_in`."""
        request_time = utc_now()
        auth_request_payload = self.oauth_request_payload
        token_response = requests.post(self.auth_endpoint, data=auth_request_payload)
        try:
            token_response.raise_for_status()
            self.logger.info("OAuth authorization attempt was successful.")
        except Exception as ex:
            raise RuntimeError(
                f"Failed OAuth login, response was '{token_response.json()}'. {ex}"
            )
        token_json = token_response.json()
        self.access_token = token_json["access_token"]
        self.logger.info(f"New access token received: {self.access_token}")
        self.expires_in = token_json["expires_in"]
        self.logger.info(f"New expires_in received: {self.expires_in}")
        self.last_refreshed = request_time
        self.logger.info(f"New last_refreshed time: {self.last_refreshed}")
        self._tap._config["access_token"] = token_json["access_token"]
        self._tap._config["expires_in"] = token_json["expires_in"]

        if token_json.get("refresh_token") is not None:
            self.refresh_token = token_json["refresh_token"]
            self._tap._config["refresh_token"] = token_json["refresh_token"]
            self.logger.info(f"New refresh_token: {self.refresh_token}")

        self.logger.info(f"New token response: {token_json}")
        with open(self._tap.config_file, "w") as outfile:
            json.dump(self._tap._config, outfile, indent=4)

    @classmethod
    def create_for_stream(cls, stream) -> BlackbaudV2Authenticator:  # noqa: ANN001
        return cls(
            stream=stream,
        )
