"""REST client handling, including BlackbaudV2Stream base class."""

from __future__ import annotations

import sys
import requests
from pathlib import Path
from typing import Any, Callable, Iterable
from urllib.parse import parse_qsl

from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.pagination import BaseAPIPaginator  # noqa: TCH002
from singer_sdk.streams import RESTStream
from singer_sdk.pagination import BaseHATEOASPaginator

from tap_blackbaud_v2.auth import BlackbaudV2Authenticator

if sys.version_info >= (3, 8):
    from functools import cached_property
else:
    from cached_property import cached_property

_Auth = Callable[[requests.PreparedRequest], requests.PreparedRequest]
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")

from tap_blackbaud_v2.paginators import BlackbaudV2Paginator

class BlackbaudV2Stream(RESTStream):
    """BlackbaudV2 stream class."""

    def __init__(self, override_parent_stream_type=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if override_parent_stream_type:
            self.parent_stream_type = override_parent_stream_type

    @property
    def url_base(self) -> str:
        return "https://api.sky.blackbaud.com"

    records_jsonpath: str = "$.value[*]"

    def get_new_paginator(self):
        return BlackbaudV2Paginator()

    @cached_property
    def authenticator(self) -> _Auth:
        return BlackbaudV2Authenticator(self)

    @property
    def http_headers(self) -> dict:
        result = super().http_headers
        result['Bb-Api-Subscription-Key'] = self._config["subscription_key"]
        return result

    def get_new_paginator(self) -> BaseAPIPaginator:
        return BlackbaudV2Paginator()

    def get_url_params(self, context, next_page_token) -> dict:
        if next_page_token:
            return dict(parse_qsl(next_page_token.query))
        return {}

    def prepare_request_payload(
        self,
        context,
        next_page_token
    ):
        """Prepare the data payload for the REST API request.

        By default, no payload will be sent (return None).

        Args:
            context: The stream context.
            next_page_token: The next page index or value.

        Returns:
            A dictionary with the JSON body for a POST requests.
        """
        # TODO: Delete this method if no payload is required. (Most REST APIs.)
        return None

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result records.

        Args:
            response: The HTTP ``requests.Response`` object.

        Yields:
            Each record from the source.
        """
        # TODO: Parse response body and return a set of records.
        yield from extract_jsonpath(self.records_jsonpath, input=response.json())

    def post_process(
        self,
        row,
        context
    ):

        return row
